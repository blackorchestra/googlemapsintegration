//
//  ViewController.swift
//  GoogleMapsIntegration
//
//  Created by ihor on 05.06.2018.
//  Copyright © 2018 blackorchestra.team. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var camera: GMSCameraPosition!
    var mapView: GMSMapView!
    var marker: GMSMarker!
    let locationManager = CLLocationManager()

    
    override func loadView() {
        super.loadView()

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        camera = GMSCameraPosition.camera(withLatitude: 48.4516474, longitude: 34.9991299, zoom: 13)
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height), camera: camera)
        view = mapView
        marker = GMSMarker(position: CLLocationCoordinate2DMake(48.4516474, 34.9991299))
        marker.map = mapView
        
        
        //let locationManager = CLLocationManager()
        //locationManager.requestAlwaysAuthorization()
        //locationManager.requestWhenInUseAuthorization()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = manager.location?.coordinate {
            print("UPDATE 1")
            getCurrentLocation(currentLocation)
        } else {
            print("UPDATE 2")
            getCurrentLocation(CLLocationCoordinate2DMake(48.4516474, 34.9991299))
        }
        
    }
    
    func getCurrentLocation(_ currentLocation: CLLocationCoordinate2D) {
        camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 10)
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height), camera: camera)
        view = mapView
        
        marker = GMSMarker(position: CLLocationCoordinate2DMake(currentLocation.latitude, currentLocation.longitude))
        marker.map = mapView
    }
}

